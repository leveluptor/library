package com.tatianomnom.library.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by tatiana on 01.02.15.
 */
@Repository
public interface BookRentalRepository extends JpaRepository<BookRental, Long> {
    BookRental findFirstByBookAndUser(Book book, User user);

    @Query("select br from BookRental br where br.book = :book and br.returnedDate is null")
    BookRental isBookAlreadyTaken(@Param("book") Book book);
}
