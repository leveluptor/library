package com.tatianomnom.library.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tatiana on 01.02.15.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
