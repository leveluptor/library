package com.tatianomnom.library.domain;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by tatiana on 01.02.15.
 */
@Entity
public class BookRental {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne
    private Book book;

    @ManyToOne
    private User user;

    private Date borrowedDate;

    private Date returnedDate;

    protected BookRental() {

    }

    public BookRental(Book book, User user, Date borrowedDate) {
        this.book = book;
        this.user = user;
        this.borrowedDate = borrowedDate;
    }

    //TODO is it OK? We need to save to DB with the same id...
    //or maybe the structure is incorrect
    public BookRental(BookRental originalBookRental, Date returnedDate) {
        this.id = originalBookRental.id;
        this.book = originalBookRental.book;
        this.user = originalBookRental.user;
        this.borrowedDate = originalBookRental.borrowedDate;
        this.returnedDate = returnedDate;
    }


    public Book getBook() {
        return book;
    }

    public User getUser() {
        return user;
    }

    public Date getBorrowedDate() {
        return borrowedDate;
    }

    public Date getReturnedDate() {
        return returnedDate;
    }

    @Override
    public String toString() {
        return "BookRental{" +
                "id=" + id +
                ", book=" + book +
                ", user=" + user +
                ", borrowedDate=" + borrowedDate +
                ", returnedDate=" + returnedDate +
                '}';
    }
}
