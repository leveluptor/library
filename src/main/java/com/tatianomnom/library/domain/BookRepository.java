package com.tatianomnom.library.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by tatiana on 01.02.15.
 */
//TODO is it ok to annotate them as repositories?
@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
}
