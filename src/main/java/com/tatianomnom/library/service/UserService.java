package com.tatianomnom.library.service;

import com.tatianomnom.library.domain.User;
import com.tatianomnom.library.domain.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tatiana on 01.02.15.
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    //TODO return void, long, User?
    public User createUser(String name) {
        User user = new User(name);
        return userRepository.save(user);
    }

    public List<User> getAll() {
        return userRepository.findAll();
    }
}
