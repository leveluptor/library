package com.tatianomnom.library.service;

import com.tatianomnom.library.domain.*;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.Calendar;
import java.util.List;

/**
 * Created by tatiana on 01.02.15.
 */
@Service
public class BookRentalService {

    private static final Logger logger = Logger.getLogger(BookRentalService.class);

    @Autowired
    private BookRentalRepository bookRentalRepository;

    public boolean isBookAvailable(Book book) {
        //check that he can take it
        BookRental probableExisting = bookRentalRepository.isBookAlreadyTaken(book);
        return probableExisting == null;
    }

    public BookRental takeBook(User user, Book book) {
        Date now = new Date(Calendar.getInstance().getTime().getTime());
        BookRental bookRental = new BookRental(book, user, now);
        return bookRentalRepository.save(bookRental);
    }

    public BookRental returnBook(User user, Book book) {
        Date now = new Date(Calendar.getInstance().getTime().getTime());
        BookRental bookRentalToUpdate = bookRentalRepository.findFirstByBookAndUser(book, user);
        BookRental bookRental = new BookRental(bookRentalToUpdate, now);
        return bookRentalRepository.save(bookRental);
    }

    public List<BookRental> getAll() {
        return bookRentalRepository.findAll();
    }
}
