package com.tatianomnom.library.service;

import com.tatianomnom.library.domain.Book;
import com.tatianomnom.library.domain.BookRental;
import com.tatianomnom.library.domain.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by tatiana on 01.02.15.
 */
@Service
public class BookService {

    @Autowired
    private BookRepository bookRepository;

    //TODO return void, long, Book?
    public Book createBook(String name) {
        Book book = new Book(name);
        return bookRepository.save(book);
    }

    public List<Book> getAll() {
        return bookRepository.findAll();
    }

}
