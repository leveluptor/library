package com.tatianomnom.library;

import com.tatianomnom.library.domain.Book;
import com.tatianomnom.library.domain.BookRental;
import com.tatianomnom.library.domain.User;
import com.tatianomnom.library.service.BookRentalService;
import com.tatianomnom.library.service.BookService;
import com.tatianomnom.library.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class LibraryApplicationTests {

    @Autowired
    private BookService bookService;

    @Autowired
    private UserService userService;

    @Autowired
    private BookRentalService bookRentalService;

    @Test
    public void bookIsCreated() {
        assertThat(bookService.getAll(), empty());
        bookService.createBook("Thinking in Java");
        assertThat(bookService.getAll().size(), equalTo(1));
    }

    @Test
    public void userIsCreated() {
        assertThat(userService.getAll(), empty());
        userService.createUser("Vasya");
        userService.createUser("Petya");
        assertThat(userService.getAll().size(), equalTo(2));
    }

    @Test
    public void booksAreTakenAndReturned() {
        Book book = bookService.createBook("Thinking in Java");
        Book book2 = bookService.createBook("Swimming in lava");
        User vasya = userService.createUser("Vasya");

        assertThat(bookRentalService.getAll(), empty());

        assertThat(bookRentalService.isBookAvailable(book), is(true));
        assertThat(bookRentalService.isBookAvailable(book2), is(true));

        BookRental bookRented = bookRentalService.takeBook(vasya, book);
        assertThat(bookRented.getBorrowedDate(), notNullValue());
        assertThat(bookRented.getReturnedDate(), nullValue());

        assertThat(bookRentalService.isBookAvailable(book), is(false));

        BookRental bookReturned = bookRentalService.returnBook(vasya, book);
        assertThat(bookReturned.getBorrowedDate(), notNullValue());
        assertThat(bookReturned.getReturnedDate(), notNullValue());

        assertThat(bookRentalService.isBookAvailable(book), is(true));

        bookRentalService.takeBook(vasya, book2);
        assertThat(bookRentalService.getAll().size(), equalTo(2));

        assertThat(bookRentalService.isBookAvailable(book2), is(false));
    }



}
